*** Settings ***
Documentation    Aqui estarão presentes os testes web do curso Prime Hero.
...              A ideia é criamos cenários de verificação com a library Selenium.

Library              SeleniumLibrary
Library              FakerLibrary    

Test Setup           Abrir Navegador
Test Teardown        Fechar Navegador

*** Variables ***
${URL}            http://automationpractice.com/index.php
${BROWSER}        chrome

*** Keywords ***

Abrir Navegador
    Open Browser                         browser=${BROWSER}
    Maximize Browser Window        
Fechar Navegador
    Capture Page Screenshot
    Close Browser 

###UC-01###
Acessar a página home do site Automation Practice
    Go To                                ${URL}
    Title Should Be                      My Store
    Wait Until Element Is Visible        id=block_top_menu

Digitar o nome do produto "${PRODUTO}" no campo de pesquisa
    Input Text                           id=search_query_top                ${PRODUTO} 

Clicar no botão pesquisar
    Click Element                        xpath=//*[@name='submit_search' and @type='submit']

Conferir se o produto "${PRODUTO}" foi listado no site
    Wait Until Element Is Visible        id=center_column
    Title Should Be                      Search - My Store
    Page Should Contain Image            xpath=//*[@id='center_column']//*[@src="http://automationpractice.com/img/p/7/7-home_default.jpg"]

###UC-02###
Conferir mensagem "No results were found for your search "${ITEMINEXISTENTE}""
    Wait Until Element Is Visible        id=center_column
    Wait Until Element Is Visible        xpath=//*[@id="center_column"]/p

###UC-03###
Acessar a página home do site
    Go To                                ${URL}
    Title Should Be                      My Store

Passar o mouse por cima da categoria "Women" no menu principal superior de categorias
    Wait Until Element Is Visible        xpath=//*[@id="block_top_menu"]/ul/li[1]/a
    Mouse Over                           xpath=//*[@id="block_top_menu"]/ul/li[1]/a

Clicar na sub categoria "Summer Dresses"
    Wait Until Element Is Visible        xpath=//*[@id="block_top_menu"]/ul/li[1]/ul/li[2]/ul/li[3]/a
    Click Element                        xpath=//*[@id="block_top_menu"]/ul/li[1]/ul/li[2]/ul/li[3]/a

Conferir se os produtos da sub-categoria "Summer Dresses" foram mostrados na página
    Wait Until Element Is Visible        xpath=//*[@id="columns"]/div[1]
    Title Should Be                      Summer Dresses - My Store
    Wait Until Element Is Visible        xpath=//*[@id="center_column"]/ul

###UC-04###
Clicar em "Sign in"
    Wait Until Element Is Visible        xpath=//*[@id="header"]/div[2]/div/div/nav/div[1]/a
    Click Element                        xpath=//*[@id="header"]/div[2]/div/div/nav/div[1]/a
Informar um e-mail válido
    Wait Until Element Is Visible        xpath=//*[@id="email_create"]
    ${EMAIL}                             FakerLibrary.Email
    Set Test Variable                    ${EMAIL}
    Input Text                           xpath=//*[@id="email_create"]         ${EMAIL}    

Clicar em "Create an account"
    Wait Until Element Is Visible        id=SubmitCreate
    Click Element                        id=SubmitCreate

Preencher os dados obrigatórios
#YOUR PERSONAL INFORMATION
#FirstName
    Wait Until Element Is Visible        xpath=//*[@id="customer_firstname"]
    ${FNAME}                             FakerLibrary.Name
    Input Text                           xpath=//*[@id="customer_firstname"]    ${FNAME}

#LastName
    Wait Until Element Is Visible        xpath=//*[@id="customer_lastname"]
    ${LNAME}                             FakerLibrary.Last Name
    Input Text                           xpath=//*[@id="customer_lastname"]     ${LNAME}

#YOUR ADDRESS
#First name preenchido automático
#Last name  preenchido automático

#Address
    Wait Until Element Is Visible        xpath=//*[@id="address1"]
    ${ADDRESS}                           FakerLibrary.Address
    Input Text                           xpath=//*[@id="address1"]              ${ADDRESS}

#City
    Wait Until Element Is Visible        xpath=//*[@id="city"]
    ${CITY}                              FakerLibrary.City
    Input Text                           xpath=//*[@id="city"]                  ${CITY}

#State
    Wait Until Element Is Visible        xpath=//*[@id="uniform-id_state"]
    Select From List By Value            xpath=//*[@id="id_state"]              5

#Zip/Postal Code 
    Wait Until Element Is Visible        xpath=//*[@id="postcode"]
    ${POSTALCODE}                        FakerLibrary.Postalcode
    Input Text                           xpath=//*[@id="postcode"]              ${POSTALCODE}

#Country
    Wait Until Element Is Visible        xpath=//*[@id="uniform-id_country"]
    Select From List By Value            xpath=//*[@id="id_country"]            21  

#Mobile phone
    Wait Until Element Is Visible        xpath=//*[@id="phone_mobile"]
    Input Text                           xpath=//*[@id="phone_mobile"]          (92) 99407-5189
    # ${PHONENUMBER}                       FakerLibrary.Phone Number
    # Input Text                           xpath=//*[@id="phone_mobile"]          ${PHONENUMBER}

#Email Address
    Wait Until Element Is Visible        xpath=//*[@id="alias"]
    Input Text                           xpath=//*[@id="alias"]                 ${EMAIL}

#Password
    Wait Until Element Is Visible        xpath=//*[@id="passwd"]
    Sleep                                5    
    ${PASSWORD}                          FakerLibrary.Password
    Input Password                       xpath=//*[@id="passwd"]                ${PASSWORD}

Submeter cadastro
    Wait Until Element Is Visible        submitAccount
    Click Element                        submitAccount
    Sleep                                5
Conferir se o cadastro foi efetuado com sucesso
    Wait Until Element Is Visible        xpath=//*[@id="center_column"]/div/div[1]/ul
    Title Should Be                      My account - My Store

###FIM-UC-04###

*** Test Cases ***
Caso de Teste 01: Pesquisar produto existente
    Acessar a página home do site Automation Practice
    Digitar o nome do produto "Blouse" no campo de pesquisa
    Clicar no botão pesquisar
    Conferir se o produto "Blouse" foi listado no site

Caso de Teste 02: Pesquisar produto não existente
    Acessar a página home do site Automation Practice
    Digitar o nome do produto "itemNãoExistente" no campo de pesquisa
    Clicar no botão pesquisar
    Conferir mensagem "No results were found for your search "itemNãoExistente""

Caso de Teste 03: Listar Produtos
    Acessar a página home do site
    Passar o mouse por cima da categoria "Women" no menu principal superior de categorias
    Clicar na sub categoria "Summer Dresses"
    Conferir se os produtos da sub-categoria "Summer Dresses" foram mostrados na página

Caso de Teste 04: Adicionar Cliente
    Acessar a página home do site
    Clicar em "Sign in"
    Informar um e-mail válido
    Clicar em "Create an account"
    Preencher os dados obrigatórios
    Submeter cadastro
    Conferir se o cadastro foi efetuado com sucesso